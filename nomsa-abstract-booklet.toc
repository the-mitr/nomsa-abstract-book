\contentsline {chapter}{\numberline {1}About}{9}{chapter.1}%
\contentsline {section}{NOMSA21 Open-Up and Connect}{9}{section*.1}%
\contentsline {section}{About NOMSA}{9}{section*.1}%
\contentsline {chapter}{\numberline {2}Introduction}{11}{chapter.2}%
\contentsline {chapter}{\numberline {3}Keynote Speakers}{13}{chapter.3}%
\contentsline {section}{Prof. Asha S. Kanwar}{14}{section*.2}%
\contentsline {section}{Prof. Maha Bali}{15}{section*.2}%
\contentsline {section}{Prof. Moeketsi Letseka}{16}{section*.2}%
\contentsline {section}{Dr. Kaviraj Sharma Sukon}{17}{Item.6}%
\contentsline {chapter}{\numberline {4}Conference Programme}{19}{chapter.4}%
\contentsline {section}{Day 1: Monday, 6 December 2021}{19}{section*.3}%
\contentsline {section}{Day 2: Tuesday, 7 December 2021}{22}{section*.3}%
\contentsline {chapter}{\numberline {5}Abstracts of Presentations}{25}{chapter.5}%
\contentsline {section}{\numberline {1}Blended Learning: Is It The Future Model Of Education}{26}{section.5.1}%
\contentsline {section}{\numberline {2}Curbing Exclusion: The Experiences Of Students With Visual Impairments And Their Lecturers On Distance And Online Learning During The Covid-19 Pandemic In Namibia}{27}{section.5.2}%
\contentsline {section}{\numberline {3}Socio-cultural Context of Learners' Identity Construction In Online and Distance Education: A Case from Nepal}{29}{section.5.3}%
\contentsline {section}{\numberline {4}The Technical Support Needs Of Distance Students To Participate In Open And Distance Learning Online Courses At The Centre Of Open And Lifelong Learning (COLL)}{30}{section.5.4}%
\contentsline {section}{\numberline {5}The Psychological Impacts Of Digital Learning On Student Performance -– An Exploratory Approach}{31}{section.5.5}%
\contentsline {section}{\numberline {6}Virtual Resources For Teaching Learning: Boon For Indian Education System}{33}{section.5.6}%
\contentsline {section}{\numberline {7}Comparative Analysis Of Nigerian University Teachers' Experiences Of Blended And Online Teaching During And After The Covid 19 Pandemic}{34}{section.5.7}%
\contentsline {section}{\numberline {8}Mobile Technologies For M-Learning As A Solution To Mitigate The Challenges Of The Online Learning In Ghana Post Covid-19}{35}{section.5.8}%
\contentsline {section}{\numberline {9}An Investigative Study Of Opinion Mining About E-Learning And Transformation Driven By Covid-19}{36}{section.5.9}%
\contentsline {section}{\numberline {10}Hybrid Learning For Sustainable Futures Of Education: Environmental, Technological, Social And Economic}{38}{section.5.10}%
\contentsline {section}{\numberline {11}Using Community Of Practice Theories To Understand The Perspectives Of e-Tutors And Lecturers In Student Support At The University Of South Africa}{39}{section.5.11}%
\contentsline {section}{\numberline {12}An Analysis Of The Impact Of Covid-19 Emergency Remote Learning On First-Year \textls [50]{LLB} Student Success Rates}{40}{section.5.12}%
\contentsline {section}{\numberline {13}e-Assessment For Multimodal Learning}{41}{section.5.13}%
\contentsline {section}{\numberline {14}An Investigation Into An An Information Communications Technology Based Chinese Studies Curriculum Design And Planning For School Certificate (Technical) Upper Secondary Schools In Mauritius}{43}{section.5.14}%
\contentsline {section}{\numberline {15}Online Group Supervision As Pedagogy Under Emergency Conditions: Optimising Online Collaboration Towards Self-Directed Learning In A South African HEI}{45}{section.5.15}%
\contentsline {section}{\numberline {16}Compounding The Effects Of 4G And Covid-19 On Adoption \\And Use Of Online-Banking: A Perspective}{47}{section.5.16}%
\contentsline {section}{\numberline {17}Role Of Supervisory Management Styles In Shaping The Supervision Experiences Of Doctoral Students}{48}{section.5.17}%
\contentsline {section}{\numberline {18}Assessment Of Digitalised Learning And Teaching \\To Senior High School In Pampanga, Philippines}{49}{section.5.18}%
\contentsline {section}{\numberline {19}Overcoming Gender Imbalance In ICT-Related Jobs}{50}{section.5.19}%
\contentsline {section}{\numberline {20}Enhancing Learning Experiences From OER Initiatives: \\Lessons From Indian Higher Education Institutions (HEIs)}{51}{section.5.20}%
\contentsline {section}{\numberline {21}Student Plagiarism In Online/Distance Higher Education}{53}{section.5.21}%
\contentsline {section}{\numberline {22}Evaluation Of Digital Educational Initiative During The Covid-19 Pandemic Using Concern Based Adoption Model}{54}{section.5.22}%
\contentsline {section}{\numberline {23}Using E-Portfolios For Active Student Engagement In \textls [50]{ODeL} Environment During And Post The Covid-19 Pandemic}{55}{section.5.23}%
\contentsline {section}{\numberline {24}Face-To-Face Versus Remote Learning Student Behaviour Analysis Through Educational Data Mining}{56}{section.5.24}%
\contentsline {section}{\numberline {25}Learning Behind A Screen During Covid-19 Pandemic!!! Students' Experiences At The Open University Of Mauritius}{57}{section.5.25}%
\contentsline {section}{\numberline {26}Using Academic E-Portfolio In Secondary Schools-Evaluating The Readiness Of Educators And Willingness Of Learners In Mauritius}{59}{section.5.26}%
\contentsline {section}{\numberline {27}The Value Of Multimodal Learning Tools To Engage Learners While Working Online}{60}{section.5.27}%
\contentsline {section}{\numberline {28}Distance, Blended And Online Natural Sciences Teacher Professional Development}{61}{section.5.28}%
\contentsline {section}{\numberline {29}Reinventing A Medical Elective Rotation In A Pandemic}{63}{section.5.29}%
\contentsline {section}{\numberline {30}A Study Of \textls [50]{MOOC}'S Learners Learning Expectations, Learning Engagement And Satisfaction}{65}{section.5.30}%
\contentsline {section}{\numberline {31}Lecturers' Recommendations On Learning Management System Training And Support To Improve The Implementation Of Blended Learning In A Higher Education Institution}{67}{section.5.31}%
\contentsline {section}{\numberline {32}Self-Directed Learning: The Rule Of A New Learning Paradigm}{68}{section.5.32}%
\contentsline {section}{\numberline {33}Sharing Professional Place Based Teaching Experiences: Podcasting For Open Teacher Professional Development}{69}{section.5.33}%
\contentsline {section}{\numberline {34}Transdisciplinary Competencies And Values Through Open Pedagogy -- Lessons Learnt}{70}{section.5.34}%
\contentsline {section}{\numberline {35}Assessing Online Learning Student Experiences Of First Year Students In The Department Of Higher Education And Lifelong Learning At The University Of Namibia}{72}{section.5.35}%
\contentsline {section}{\numberline {36}Conceptualisation Of OERs By Lecturers At Selected South African Universities}{73}{section.5.36}%
\contentsline {section}{\numberline {37}The Students' Perspectives On Peer Assessment As Tools For Emotional And Academic Support During Remote Online Teaching And Learning}{75}{section.5.37}%
\contentsline {section}{\numberline {38}Educators' Beliefs, Perceptions And Practices Regarding Open Education Resources And Assessment Models}{76}{section.5.38}%
\contentsline {section}{\numberline {39}Assessment Of E-Addiction And Online Learning Deception Among Students At Higher Education Institutions In Namibia}{78}{section.5.39}%
\contentsline {section}{\numberline {40}Reflective Teaching And Learners' Performance: The Case Of A Selected Region In Namibia}{79}{section.5.40}%
\contentsline {section}{\numberline {41}Make Learning Enjoyable By Gamifying Blended Learning Courses In Technical And Vocational Education And Training}{80}{section.5.41}%
\contentsline {section}{\numberline {42}How Can You Use Open Educational Resources (\textls [50]{OER}) To Design Alternative Assessment In Engineering Courses}{81}{section.5.42}%
\contentsline {section}{\numberline {43}The Influence Of Distance And Online Learning During Covid-19 Lockdown On Student-Lecturer Interaction In Namibia}{82}{section.5.43}%
\contentsline {section}{\numberline {44}A Review Of OER Research In Southern Africa}{84}{section.5.44}%
\contentsline {section}{\numberline {45}Flipped Classroom Approach In The Digital Post-Covid Era: An EFL Blended Learning Scenario That Promotes Healthy Eating Habits}{85}{section.5.45}%
\contentsline {section}{\numberline {46}The Fellowship Of The OER Ring: How To Tackle The Dark Lord Of Copyright}{86}{section.5.46}%
\contentsline {section}{\numberline {47}The Relationship Between Online Professional Learning Communities And Online Presence}{87}{section.5.47}%
\contentsline {section}{\numberline {48}A Qualitative Analysis of Challenges to Open Distance Learning in Gabon}{88}{section.5.48}%
\contentsline {section}{\numberline {49}Introducing A Hybrid Science Learning Model In Local Communities Through Inquiry And Self Directed Learning}{89}{section.5.49}%
\contentsline {section}{\numberline {50}How To Create Commons: An Overview Of Assorted Libre Creation Tools}{90}{section.5.50}%
\contentsline {section}{\numberline {51}Puzzle Based Learning For An Online Teaching And Learning Environment}{91}{section.5.51}%
\contentsline {section}{\numberline {52}The Open Science Framework As A Pathway To Improved Teacher Education}{92}{section.5.52}%
\contentsline {chapter}{\numberline {6}Name Index}{95}{chapter.6}%
\contentsfinish 
